<?php

namespace Drupal\apple_store_server_notifications\Model;

/**
 * Apple store server notification.
 */
class Notification extends ModelBase {

  /**
   * Notification's UUID.
   *
   * @var string
   */
  protected string $uuid;

  /**
   * Notification type.
   *
   * @var string
   */
  protected string $notificationType;

  /**
   * Notification subtype.
   *
   * @var string
   */
  protected ?string $subtype;

  /**
   * Renewal information.
   *
   * @var RenewalInfo
   */
  protected RenewalInfo $renewalInfo;

  /**
   * Transaction information.
   *
   * @var TransactionInfo
   */
  protected TransactionInfo $transactionInfo;

  /**
   * Event constructor.
   *
   * @param string $uuid
   *   Notification UUID.
   * @param string $notificationType
   *   Notification type.
   * @param TransactionInfo $transactionInfo
   *   Transaction information.
   * @param RenewalInfo $renewalInfo
   *   Renewal information.
   * @param string $subtype
   *   Notification subtype.
   */
  public function __construct(string $uuid, string $notificationType, TransactionInfo $transactionInfo, RenewalInfo $renewalInfo, string $subtype = NULL) {
    $this->uuid = $uuid;
    $this->notificationType = $notificationType;
    $this->subtype = $subtype;
    $this->renewalInfo = $renewalInfo;
    $this->transactionInfo = $transactionInfo;
  }

  /**
   * Gets the notification type.
   *
   * @return string
   *   Notification type.
   */
  public function getNotificationType() {
    return $this->notificationType;
  }

  /**
   * Gets the notification subtype.
   *
   * @return string
   *   Notification subtype.
   */
  public function getSubtype() {
    return $this->subtype;
  }

  /**
   * Get renewal information.
   *
   * @return RenewalInfo
   *   Renewal information.
   */
  public function getRenewalInfo() : RenewalInfo {
    return $this->renewalInfo;
  }

  /**
   * Get transaction information.
   *
   * @return TransactionInfo
   *   Transaction information.
   */
  public function getTransactionInfo() {
    return $this->transactionInfo;
  }

  /**
   * Get a notification's UUID.
   *
   * @return string
   *   Notification's UUID.
   */
  public function getNotificationUuid() {
    return $this->uuid;
  }

}
