<?php

namespace Drupal\apple_store_server_notifications\Model;

/**
 * Information about an apple transaction.
 */
class TransactionInfo extends ModelBase {

  /**
   * The transaction identifier of a purchase.
   *
   * @var int
   */
  protected $transactionId;

  /**
   * The original transaction identifier of a purchase.
   *
   * @var int
   */
  protected $originalTransactionId;

  /**
   * The unique identifier of subscription purchase events across devices.
   *
   * @var string
   */
  protected $webOrderLineItemId;

  /**
   * The bundle identifier of the app.
   *
   * @var string
   */
  protected $bundleId;

  /**
   * The product identifier of the in-app purchase.
   *
   * @var string
   */
  protected $productId;

  /**
   * Subscription group identifier.
   *
   * @var string
   */
  protected $suscriptionGroupIdentifier;

  /**
   * The number of consumable products the user purchased.
   *
   * @var int
   */
  protected $quantity;

  /**
   * The type of the in-app purchase.
   *
   * @var string
   */
  protected $type;

  /**
   * Whether the transaction was purchased / family sharing.
   *
   * @var string
   */
  protected $inAppOwnerShipType;

  /**
   * Environment.
   *
   * @var string
   */
  protected $environment;

  /**
   * Gets the transaction id.
   *
   * @return int
   *   Transaction ID.
   */
  public function getTransactionId() {
    return $this->transactionId;
  }

  /**
   * Gets the original transaction id.
   *
   * @return int
   *   Original transaction ID.
   */
  public function getOriginalTransactionid() {
    return $this->originalTransactionId;
  }

  /**
   * Gets the line item ID.
   *
   * @return int
   *   Line item ID.
   */
  public function getWebOrderLineItemId() {
    return $this->webOrderLineItemId;
  }

  /**
   * Gets the bundle ID.
   *
   * @return string
   *   Bundle Id.
   */
  public function getBundleId() : string {
    return $this->bundleId;
  }

  /**
   * Gets the product id.
   *
   * @return string
   *   Product id.
   */
  public function getProductId() : string {
    return $this->productId;
  }

  /**
   * Gets the group identifier.
   *
   * @return string
   *   Group identifier.
   */
  public function getSuscriptionGroupIdentifier() : string {
    return $this->suscriptionGroupIdentifier;
  }

  /**
   * Gets the quantity.
   *
   * @return int
   *   Quantity.
   */
  public function getQuantity() : int {
    return $this->quantity;
  }

  /**
   * Gets the type of transaction.
   *
   * @return string
   *   Get transaction type.
   */
  public function getType() : string {
    return $this->type;
  }

  /**
   * Gets the procedence of the ownership.
   *
   * @return string
   *   Owned by family sharing or by user's purchase.
   */
  public function getInAppOwnerShipType() : string {
    return $this->inAppOwnerShipType;
  }

  /**
   * Gets the environment.
   *
   * @return string
   *   Environment.
   */
  public function getEnvironment() : string {
    return $this->environment;
  }

}
