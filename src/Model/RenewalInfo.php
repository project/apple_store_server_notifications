<?php

namespace Drupal\apple_store_server_notifications\Model;

/**
 * Renewal information that comes from an event.
 *
 * @see https://developer.apple.com/documentation/appstoreservernotifications/jwsrenewalinfodecodedpayload
 */
class RenewalInfo extends ModelBase {

  /**
   * Environment of reneval (sandbox / production).
   *
   * @var string
   */
  protected string $environment;

  /**
   * The reason a subscription expired.
   *
   * @var int
   */
  protected $expirationIntent;

  /**
   * The original transaction identifier of a purchase.
   *
   * @var int
   */
  protected $originalTransactionId;

  /**
   * The product identifier of the in-app purchase.
   *
   * @var string
   */
  protected $productId;

  /**
   * The renewal status for an auto-renewable subscription.
   *
   * @var int
   */
  protected $autoRenewStatus;

  /**
   * Indicates App Store tries to automatically renew an expired subscription.
   *
   * @var bool
   */
  protected $isInBillingRetryPeriod;

  /**
   * Gets the environment.
   *
   * @return string
   *   Environment.
   */
  public function getEnvironment() {
    return $this->environment;
  }

  /**
   * Gets the expiration intent.
   *
   * @return int
   *   Expiration intent.
   */
  public function getExpirationIntent() {
    return $this->environment;
  }

  /**
   * Gets the original transaction ID.
   *
   * @return int
   *   Original transaction ID.
   */
  public function getOriginalTransactionid() {
    return $this->originalTransactionId;
  }

  /**
   * Gets the product id.
   *
   * @return string
   *   Product ID.
   */
  public function getProductId() {
    return $this->productId;
  }

  /**
   * Gets the auto renewal status.
   *
   * @return int
   *   Auto renewal status.
   */
  public function getAutoRenewalStatus() {
    return $this->autoRenewStatus;
  }

  /**
   * Checks if apple tries to renew a subscription.
   *
   * @return string
   *   TRUE when apple tries to renew a subscription.
   */
  public function getIsInBillingRetryPeriod() {
    return $this->isInBillingRetryPeriod;
  }

}
