<?php

namespace Drupal\apple_store_server_notifications\Model;

/**
 * Base of all apple object models.
 */
abstract class ModelBase {

  /**
   * Instance the model from an object.
   *
   * @param object $object
   *   Raw object coming from the response body.
   */
  public static function createFromObject(\stdClass $object) {
    $instance = new static();
    foreach (get_object_vars($object) as $property => $value) {
      $instance->{$property} = $value;
    }
    return $instance;
  }

}
