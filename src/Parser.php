<?php

namespace Drupal\apple_store_server_notifications;

use Drupal\apple_store_server_notifications\Exception\InvalidSignedDataException;
use Drupal\apple_store_server_notifications\Model\Notification;
use Drupal\apple_store_server_notifications\Model\RenewalInfo;
use Drupal\apple_store_server_notifications\Model\TransactionInfo;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\webhooks\Exception\WebhookMismatchSignatureException;
use Jose\Algorithm\SignatureAlgorithmInterface;
use Jose\Factory\AlgorithmManagerFactory;
use Jose\KeyConverter\KeyConverter;
use Jose\Object\JWK;
use Firebase\JWT\JWT;

/**
 * Safe parser of the events incoming from apple server.
 */
class Parser {

  /**
   * App server configuration used to get the thumbprint.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Constructs the parser.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->config = $configFactory->get('apple_store_server_notifications.settings');
  }

  /**
   * Gets the thumbprint of the incoming event.
   *
   * @return string
   *   Thumbprint.
   */
  protected function getThumbprintFromSettings() {
    return $this->config->get('x509_thumbprint');
  }

  /**
   * Extracts theJWT parts.
   *
   * @return array
   *   Array with the header, the payload, and the signature.
   */
  protected function extractJwtParts(string $signedData) {

    $tks = \explode('.', $signedData);

    if (count($tks) != 3) {
      throw new InvalidSignedDataException('Unable to get the JWT parts.');
    }

    return $tks;
  }

  /**
   * Gets the JSON web key form JWT encoded data.
   *
   * @return \Jose\Object\JWK
   *   JWK generated.
   */
  public function getJwkFromSignedData(string $signedData) : JWK {

    list($headb64) = $this->extractJwtParts($signedData);

    $header = JWT::jsonDecode(JWT::urlsafeB64Decode($headb64));

    $x5c_pem_decoded = KeyConverter::loadFromX5C($header->x5c);
    return new JWK($x5c_pem_decoded);
  }

  /**
   * Parses a signed data.
   *
   * @return object
   *   Decoded data.
   */
  protected function parseSignedData(string $signedData) {

    $jwk = $this->getJwkFromSignedData($signedData);
    list($headb64, $bodyb64, $cryptob64) = $this->extractJwtParts($signedData);

    $manager = AlgorithmManagerFactory::createAlgorithmManager(['ES256']);

    $algorithm = $manager->getAlgorithm('ES256');

    if ($algorithm instanceof SignatureAlgorithmInterface && $algorithm->verify($jwk, $headb64 . '.' . $bodyb64, JWT::urlsafeB64Decode($cryptob64))) {
      if ($jwk->toPublic()->thumbprint('sha256') == $this->getThumbprintFromSettings()) {
        return JWT::jsonDecode(JWT::urlsafeB64Decode($bodyb64));
      }
      else {
        throw new WebhookMismatchSignatureException($jwk->toPublic()->thumbprint('sha256'), $this->getThumbprintFromSettings(), $signedData);
      }
    }
    throw new InvalidSignedDataException('The signed payload could not be signed properly.');
  }

  /**
   * Parses a signed payload to build all the event information.
   *
   * @param string $signedPayload
   *   Signed payload coming from the apple notification.
   *
   * @return \Drupal\apple_store_server_notifications\Model\Notification
   *   Apple notification.
   */
  public function parseSignedPayload(string $signedPayload) {
    $payload = $this->parseSignedData($signedPayload);
    $signedTransactionInfo = $this->parseSignedTransactionInfo($payload->data->signedTransactionInfo);
    $signedRenewalInfo = $this->parseSignedRenewalInfo($payload->data->signedRenewalInfo);
    $subtype = !empty($payload->subtype) ? $payload->subtype : NULL;
    return new Notification($payload->notificationUUID, $payload->notificationType, $signedTransactionInfo, $signedRenewalInfo, $subtype);
  }

  /**
   * Parses the signed transaction information.
   *
   * @return \Drupal\apple_store_server_notifications\Model\TransactionInfo
   *   Transaction information.
   */
  protected function parseSignedTransactionInfo(string $signedTransactionInfo) : TransactionInfo {
    $transaction_raw = $this->parseSignedData($signedTransactionInfo);
    return TransactionInfo::createFromObject($transaction_raw);
  }

  /**
   * Parses the signed renewal information.
   *
   * @return \Drupal\apple_store_server_notifications\Model\RenewalInfo
   *   Renewal information.
   */
  protected function parseSignedRenewalInfo(string $signedRenewalInfo) : RenewalInfo {
    $renewal_raw = $this->parseSignedData($signedRenewalInfo);
    return RenewalInfo::createFromObject($renewal_raw);
  }

}
