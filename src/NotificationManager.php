<?php

namespace Drupal\apple_store_server_notifications;

use Drupal\apple_store_server_notifications\Exception\InvalidSignedDataException;
use Drupal\apple_store_server_notifications\Model\Notification;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\webhooks\Event\ReceiveEvent;
use Drupal\webhooks\Event\WebhookEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Receive and distpatchs apple events.
 */
class NotificationManager implements EventSubscriberInterface {

  const EVENT_NAME = 'apple_store_server_notifications.notification';

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * Module settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Parses the incoming webhok payloads.
   *
   * @var Parser
   */
  protected Parser $parser;

  /**
   * Logs events and thumbprint.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $loggerChannel;

  /**
   * Checks notification are not duplicated.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * Constructs an EventManager object.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration factory.
   * @param Parser $parser
   *   Parser.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $loggerChannel
   *   Logger channel.
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   */
  public function __construct(EventDispatcherInterface $event_dispatcher, ConfigFactoryInterface $configFactory, Parser $parser, LoggerChannelInterface $loggerChannel, Connection $database) {
    $this->eventDispatcher = $event_dispatcher;
    $this->config = $configFactory->get('apple_store_server_notifications.settings');
    $this->parser = $parser;
    $this->loggerChannel = $loggerChannel;
    $this->database = $database;
  }

  /**
   * Method called when a webhook comes.
   *
   * @param \Drupal\webhooks\Event\ReceiveEvent $event
   *   Event.
   */
  public function onWebhookReceive(ReceiveEvent $event) {
    if ($event->getWebhookConfig()->id() == $this->config->get('webhook_config_id')) {
      try {
        $this->handlePayload($event->getWebhook()->getPayload());
      }
      catch (\JsonException $exception) {
        throw new BadRequestHttpException('Invalid JSON received into incoming webhook: ' . $exception->getMessage());
      }
      catch (InvalidSignedDataException $exception) {
        throw new AccessDeniedHttpException('Invalid signed data received: ' . $exception->getMessage());
      }
      catch (\InvalidArgumentException $exception) {
        throw new AccessDeniedHttpException('Invalid data received: ' . $exception->getMessage());
      }
    }
  }

  /**
   * Handle the payload from the apple request.
   *
   * @param array $payload
   *   Received webhook's payload.
   */
  public function handlePayload(array $payload) {
    $notification = $this->getNotificationFromPayload($payload);
    if ($notification instanceof Notification && $this->validNotification($notification)) {
      $this->dispatchNotification($notification);
      $this->markNotificationAsProcessed($notification);
    }
  }

  /**
   * Dispatchs the apple notification.
   *
   * @param \Drupal\apple_store_server_notifications\Model\Notification $notification
   *   Notification.
   */
  public function dispatchNotification(Notification $notification) {
    $event = new NotificationEvent($notification);
    $this->eventDispatcher->dispatch($event, self::EVENT_NAME);
  }

  /**
   * Call this function out of webhooks to force sending events.
   *
   * @param array $payload
   *   Payload.
   */
  public function getNotificationFromPayload(array $payload) {
    $this->logThumbprint($payload);
    if (!empty($payload['signedPayload'])) {
      $notification = $this->parser->parseSignedPayload($payload['signedPayload']);
      $this->logNotification($notification);
      return $notification;
    }
    return NULL;
  }

  /**
   * Check the notification is valid.
   *
   * @return bool
   *   TRUE when the bundle ID belongs to the allowed bundles for the site.
   */
  public function validNotification(Notification $notification) {
    return in_array($notification->getTransactionInfo()->getBundleId(), $this->config->get('allowed_bundles') ?? [])
      && !$this->isNotificationProcessed($notification);
  }

  /**
   * Logs the x509 thumbprint sent in the JWT payload.
   *
   * @param array $payload
   *   Incoming payload from apple notification.
   */
  protected function logThumbprint(array $payload) {
    if ($this->config->get('log_thumbprint')) {
      $jwk = $this->parser->getJwkFromSignedData($payload['signedPayload']);
      $this->loggerChannel->info('Apple server notification received with the following thumbprint: ' . $jwk->thumbprint('sha256'));
    }
  }

  /**
   * Logs a notification from apple.
   *
   * @param \Drupal\apple_store_server_notifications\Model\Notification $notification
   *   Notification.
   */
  protected function logNotification(Notification $notification) {
    if ($this->config->get('log_notifications')) {
      $this->loggerChannel->info('New notification received. Notification type: ' . $notification->getNotificationType()
        . '; Subtype: ' . $notification->getSubtype()
        . '; Transaction ID: ' . $notification->getTransactionInfo()->getTransactionId()
        . '; Original transaction ID: ' . $notification->getTransactionInfo()->getOriginalTransactionid()
        . '; App ID: ' . $notification->getTransactionInfo()->getBundleId() . '.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // The subscription has a lower preference just to
    // hook after the webhook module stores the webhook in database.
    // Otherwise, if there is an error at this stage, the webhook wouldn't be
    // saved.
    return [
      WebhookEvents::RECEIVE => ['onWebhookReceive', -1],
    ];
  }

  /**
   * Check that the notification is processed.
   *
   * @param \Drupal\apple_store_server_notifications\Model\Notification $notification
   *   Notification.
   *
   * @return bool
   *   TRUE when the notification has been already processed.
   */
  protected function isNotificationProcessed(Notification $notification) {
    $query = $this->database->select('apple_store_notifications_index', 'index');
    $query->addExpression('1');
    $query->condition('notification_uuid', $notification->getNotificationUuid());
    return (bool) $query->execute()->fetchField();
  }

  /**
   * Marks the notification as processed.
   *
   * @param \Drupal\apple_store_server_notifications\Model\Notification $notification
   *   Notification.
   */
  protected function markNotificationAsProcessed(Notification $notification) {
    $this
      ->database
      ->insert('apple_store_notifications_index')
      ->fields(['notification_uuid' => $notification->getNotificationUuid()])
      ->execute();
  }

}
