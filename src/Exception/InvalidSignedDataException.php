<?php

namespace Drupal\apple_store_server_notifications\Exception;

/**
 * Error produced when a signed data is not correct.
 */
class InvalidSignedDataException extends \RuntimeException {
}
