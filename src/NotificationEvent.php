<?php

namespace Drupal\apple_store_server_notifications;

use Drupal\apple_store_server_notifications\Model\Notification;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event send when an apple notification is received.
 */
class NotificationEvent extends Event {

  /**
   * Notification received by apple.
   *
   * @var \Drupal\apple_store_server_notifications\Model\Notification
   */
  protected Notification $notification;

  /**
   * Constructs the event.
   *
   * @param \Drupal\apple_store_server_notifications\Model\Notification $notification
   *   Notification.
   */
  public function __construct(Notification $notification) {
    $this->notification = $notification;
  }

  /**
   * Get the notification.
   *
   * @return \Drupal\apple_store_server_notifications\Model\Notification
   *   Notification.
   */
  public function getNotification() : Notification {
    return $this->notification;
  }

}
