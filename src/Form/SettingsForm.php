<?php

namespace Drupal\apple_store_server_notifications\Form;

use Drupal\Core\Config\ConfigBase;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Apple store server notifications settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'apple_store_server_notifications_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['apple_store_server_notifications.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('apple_store_server_notifications.settings');
    $form['x509_thumbprint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('X509 thumbprint'),
      '#default_value' => $config->get('x509_thumbprint'),
    ];

    $form['webhook_config_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webhook config ID'),
      '#default_value' => $config->get('webhook_config_id'),
    ];

    $form['allowed_bundles'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Allowed bundles'),
      '#default_value' => implode('', $this->getAllowedBundlesFromConfig($config)),
    ];

    $form['log_thumbprint'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug thumbprint'),
      '#default_value' => $config->get('log_thumbprint'),
    ];

    $form['log_notifications'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug notifications'),
      '#default_value' => $config->get('log_notifications'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('apple_store_server_notifications.settings')
      ->set('x509_thumbprint', $form_state->getValue('x509_thumbprint'))
      ->set('webhook_config_id', $form_state->getValue('webhook_config_id'))
      ->set('allowed_bundles', $this->getAllowedBundlesFromState($form_state))
      ->set('log_thumbprint', $form_state->getValue('log_thumbprint'))
      ->set('log_notifications', $form_state->getValue('log_notifications'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Get the allowed bundles from the stored configuration.
   *
   * @param \Drupal\Core\Config\ConfigBase $config
   *   Module's configuration.
   */
  protected function getAllowedBundlesFromConfig(ConfigBase $config) {
    $allowed_bundles = $config->get('allowed_bundles');
    return !empty($allowed_bundles) ? $allowed_bundles : [];
  }

  /**
   * Get the allowed bundles from the form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   */
  protected function getAllowedBundlesFromState(FormStateInterface $formState) {
    $allowed_bundles = $formState->getValue('allowed_bundles');
    return !empty($allowed_bundles) ? explode("\r\n", $allowed_bundles) : [];
  }

}
